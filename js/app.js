/*PRACTICA 4: ARRAYS
JOSE ALBERTO BARRÓN VARGAS TCI 7-2  18-Octubre-2022 */
var arreglo = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]

console.log("Arreglo: "+arreglo);

function promedio(arreglo){
    let suma=0;
    
    for (let cons=0 ; cons<20 ; cons++){
        suma = suma + (arreglo[cons]);
    }

    suma = suma/20;
    return suma;
}
console.log("El promedio es: " + promedio(arreglo));




function pares(arreglo){
    let suma = 0;
    for (let i = 0; i < 20; i++) {
        if (arreglo[i] % 2 == 0){
            suma = (suma + 1); 
        }
    }

    return suma;
}
console.log("Cantidad de valores pares en el arreglo: "+ pares(arreglo));




function orden(arreglo){
    let suma = 0;
    
    return arreglo.sort(function(a, b) {
        return b - a;
    });
}
console.log("Orden de mayor a menor: "+orden(arreglo));



function llenar(){
    var limite = document.getElementById('limite').value;
    var Listanumeros = document.getElementById('numeros');
    var myArray = [];
    let aleatorio;

    for(let i = 1; i <= limite; i++) { 
        myArray.push(i);
    }
    myArray=ascendente(myArray);

    for(let con = 0 ; con < limite ; con++){
        Listanumeros.options[con] = new Option(myArray[con]);

    }

    
}


function ascendente(numero){

    return numero.sort(function(a, b) {
        return a - b;
    });

}



//Contar los numeros pares y numeros impares para saber el porcentaje
//  y la diferencia no sea mayor al 25% 

function pares(){
    var pares = [];
    var impares = [];
    var ppares = 0.0;
    var pimpares = 0.0;

    for(let i= 0;i<Listanumeros.length;i++){
        if(i % 2 ==0){
            pares.push(Listanumeros[i]);
        }
        else{
            impares.push(Listanumeros[i]);
        }
    }

    ppares = pares / Listanumeros.value;
    pimpares = impares / Listanumeros.value;
    
    document.getElementById('ppares');
    document.getElementById('pimpares');
}




